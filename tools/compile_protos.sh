
# compile proto file to corresponding stubs - 

if [ -z "$1" ]; then
    echo usage: compile_protos.sh [proto_file_to_compiled.proto]
else
    mkdir grpc_interface
    touch ./grpc_interface/__init__.py

    PROTODIR=protos

    echo compiling $1 to python
    python -m grpc_tools.protoc -I $PROTODIR --python_out=./grpc_interface --grpc_python_out=./grpc_interface $1

    # this is for javascript compilation - uncomment, if you want to use it
    #echo compiling $1 to javascript
    #protoc -I=$PROTODIR $1 --js_out=import_style=commonjs:./grpc_interface --grpc-web_out=import_style=commonjs,mode=grpcwebtext:./grpc_interface


    # fixing import error
    cd grpc_interface
    sed -i "s/^\(import.*pb2\)/from . \1/g" *.py
fi