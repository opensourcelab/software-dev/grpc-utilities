#!/usr/bin/env python
"""Tests for `grpc_utilities` package."""
# pylint: disable=redefined-outer-name
from grpc_utilities import __version__


def test_version():
    """Sample pytest test function."""
    assert __version__ == "0.1.0"
