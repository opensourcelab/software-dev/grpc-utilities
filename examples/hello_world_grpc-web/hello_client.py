"""The Python implementation of the GRPC helloworld.Greeter client."""

import grpc

import helloworld_pb2
import helloworld_pb2_grpc


def run():
  channel = grpc.insecure_channel('localhost:50051')
  
  stub = helloworld_pb2_grpc.GreeterStub(channel)
  
  response = stub.SayHello(helloworld_pb2.HelloRequest(name='Ben'))
  print("Greeter client received: " + response.message)
  
  response = stub.SayRepeatHello(helloworld_pb2.RepeatHelloRequest(name='repBen', count=4))
  
  for resp in response:
    print("Greeter client received: " + resp.message)


if __name__ == '__main__':
  run()
