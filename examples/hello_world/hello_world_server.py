# Copyright 2015 gRPC authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""The Python implementation of the GRPC helloworld.Greeter server."""

from concurrent import futures
import time

import grpc

import grpc_interface.helloworld_pb2 as helloworld_pb2
import grpc_interface.helloworld_pb2_grpc as helloworld_pb2_grpc

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class HelloWorldServicer(helloworld_pb2_grpc.HelloWorldServicer):

    def SayHello(self, request, context):
        return helloworld_pb2.HelloReply(message=f"Hello folks, {request.name}!")

    def SayHelloAgain(self, request, context):
        message = f"Hello again my folk, { 2* request.name}!"
        return helloworld_pb2.HelloReply(message=message)


def serve():
    port = 50051
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    helloworld_pb2_grpc.add_HelloWorldServicer_to_server(
        HelloWorldServicer(), server)
    server.add_insecure_port(f'[::]:{port}')
    server.start()

    print(
        f"Hello world gRPC server started at port {port}\n press ctrl-C to terminate !")

    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()
