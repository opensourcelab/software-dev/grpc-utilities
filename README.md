# gRPC-utilities

OpensourceLab gRPC utilities for a faster start to gRPC.



## Features

* python and bash gRPC compiler scripts
* gRPC examples 

## Installation

    # create virtual environment
    python -m venv $(HOME)/py3venv/grpc-env
    
    # activate the virtual environment
    source $(HOME)/py3venv/grpc-env/bin/activate
    
    # install the required python packages 
    pip install grpcio grpcio-tools

    # or 

    # grpc-utilities installation

    # clone grc-utilities repository
    git clone https://gitlab.com/opensourcelab/software-dev/grpc-utilities.git

    # install the utilities 
    cd grpc-untilities
    pip install .



## Step-by-step tutorial

    # create a new directories for your gRPC project and change into the protos folder
    mkdir -p my_grpc_project/protos 
    cd my_grpc_project/protos

    # edit protofiles

    # compile protofiles
    grpc-python-compiler -p [my_proto_file.proto]

    # design and make server code 

    # design and make client code

    # start server
    python my_grpc_server.py

    # start client - open a new shell and type

    python my_grpc_server.py


## gRPC resources

* [https://grpc.io](https://grpc.io)
* [grpc on Youtube](https://www.youtube.com/watch?v=Yw4rkaTc0f8)
* [grpc python on Youtube](https://www.youtube.com/watch?v=WB37L7PjI5k)
* [Awsome gRPC on github](https://github.com/grpc-ecosystem/awesome-grpc)


## Credits

This package was created with Cookiecutter_ and the (opensource/templates/cookiecutter-pypackage)[ https://gitlab.com/markdoerr/cookiecutter-pypackage] project template.

   Cookiecutter: https://github.com/audreyr/cookiecutter


