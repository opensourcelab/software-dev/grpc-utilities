"""Console script for grpc_utilities."""

import sys
import os
import argparse
import logging

from typing import List

from grpc_tools import protoc


def compile_proto(proto_file: str,
                  protos_path: str = '.', output_dir: str = '.',
                  include_dirs: List[str] = None, auto_include_library: bool = True) -> bool:

    if include_dirs is None:
        include_dirs = []

    # if auto_include_library:
    #     # add the path the the SilaFramework.proto path as well as the SiLABinaryTransfer.proto
    #     include_dirs.append(os.path.join(os.path.dirname(__file__), '..', 'framework', 'protobuf'))

    # for path in include_dirs:
    #     command.append(f"--proto_path='{protos_path}'")

    # if protoc.main(command) != 0:
    #     logging.error(
    #         f'Failed to compile .proto code for from file "{proto_file}" using the command `{command}`'
    #     )
    #     return False
    # else:
    #     logging.info(
    #         f'Successfully compiled "{proto_file}"'
    #     )
    os.mkdir(output_dir)

    protoc.main([
        'grpc_tools.protoc',
        # '-I{}'.format(proto_include),
        f'--proto_path={protos_path}',
        f'--python_out={output_dir}',
        f'--grpc_python_out={output_dir}',
        f'{proto_file}'
    ])

    (pb2_files, _) = os.path.splitext(os.path.basename(proto_file))
    pb2_file = pb2_files + '_pb2.py'
    pb2_grpc_file = pb2_files + '_pb2_grpc.py'
    pb2_module = pb2_files + '_pb2'
    pb2_file = os.path.join(output_dir, pb2_file)
    pb2_grpc_file = os.path.join(output_dir, pb2_grpc_file)
    with open(pb2_grpc_file, 'r', encoding='utf-8') as file_in:
        logging.debug(f'\nCorrecting import statement of {pb2_grpc_file}')
        replaced_text = file_in.read()
        replaced_text = replaced_text.replace(f'import {pb2_module}', f'from . import {pb2_module}')
    with open(pb2_grpc_file, 'w', encoding='utf-8') as file_out:
        file_out.write(replaced_text)


def parse_command_line():
    """ Looking for command line arguments"""

    description = "gRPC python compiler"
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("_", nargs="*")

    parser.add_argument('project-dir', action='store', default='.', metavar='PROJECT_DIR', nargs='?',
                        help='Project directory from which to read all input files for code generation. '
                        + '(default: . [current directory])')
    parser.add_argument('--protos-path', action='store', default='protos',  # default=os.path.join(os.getcwd(), 'protos'),
                        help='protobuf .proto directory ')
    parser.add_argument('-p', '--proto-file', action='store', default=None,
                        help='protobuf .proto directory ')
    parser.add_argument('-o', '--output-dir', action='store', default='grpc_interface',
                        help='Main output directory for the generated code. If not specified will be chosen '
                             'depending on the job at hand.')

    # general optional arguments
    #parser.add_argument('-v', '--version', action='version', version='%(prog)s ' + __version__)

    return parser


def main():
    """Console script for grpc_utilities."""
    # or use logging.INFO (=20) or logging.ERROR (=30) for less output
    logging.basicConfig(
        format='%(levelname)-4s| %(module)s.%(funcName)s: %(message)s', level=logging.DEBUG)

    parser = parse_command_line()
    args = parser.parse_args()

    print("curr wd:", os.getcwd())

    if len(sys.argv) == 1:
        logging.error("no arguments provided !")
        parser.print_help(sys.stderr)
        sys.exit(1)
    elif args.proto_file:
        print(f"compile_proto( {args.proto_file} {args.output_dir} )")
        compile_proto(protos_path=args.protos_path, proto_file=args.proto_file, output_dir=args.output_dir)

    return 0


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
