"""Top-level package for gRPC utilities."""

__author__ = """mark doerr"""
__email__ = "mark@uni-greifswald.de"
__version__ = "0.1.0"
